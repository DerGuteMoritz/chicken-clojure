(use chicken-syntax clojure matchable setup-api posix-extras)

(define bootstrap? #f)
(define source-files '())
(define output-dir "out")
(define include-path (resolve-pathname "."))

(define (usage)
  (fprintf (current-error-port)
           "usage: ~A [-b] [-o <output-dir>] <source-file> ...~%"
           (pathname-file (program-name)))
  (exit 1))

(let loop ((args (command-line-arguments)))
  (match args
    (() (usage))
    (("-b" rest ...)
     (set! bootstrap? #t)
     (loop rest))
    (("-o" rest ...)
     (when (null? rest)
       (usage))
     (set! output-dir (car rest))
     (loop (cdr rest)))
    ((files ...)
     (set! source-files files))))

(when (null? source-files)
  (usage))

(clojure-init! bootstrap?)

(create-directory output-dir #t)
(set! output-dir (resolve-pathname output-dir))
(set! source-files (map resolve-pathname source-files))
(current-directory output-dir)

(define scheme-files
  (append-map
   (lambda (source-file)
     (clojure-compile-file source-file "."))
   source-files))

(for-each
 (lambda (scheme-file)
   (compile -no-compiler-syntax -I ,include-path -s ,scheme-file -J))
 scheme-files)
