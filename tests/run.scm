(use test srfi-69)
(use clojure)

(define (clojure-reset!)
  (clojure-namespace-remove! 'user)
  (clojure-init!)
  #t)

(define (clojure-parse strs)
  (let ((source (string-intersperse strs)))
    (values
     source
     (call-with-input-string source
       (lambda (p)
         (read-file p clojure-read))))))

(define-syntax clojure-test
  (syntax-rules ()
    ((_ clj (source eval) body ...)
     (begin
       (clojure-reset!)
       (receive (source forms)
           (clojure-parse clj)
         (let ((eval (lambda ()
                       (fold (lambda (x r)
                               (clojure-eval x))
                             (begin)
                             forms))))
           body ...))))))

(define-syntax test*
  (syntax-rules ()
    ((_ exp clj ...)
     (clojure-test
      '(clj ...)
      (source eval)
      (test source exp (eval))))))

(define-syntax test-error*
  (syntax-rules ()
    ((_ clj ...)
     (clojure-test
      '(clj ...)
      (source eval)
      (test-error source (eval))))))

(test-begin "clojure")

(test-group "misc"
  (test* 3 "(+ 1 2)")
  (test* 3 "(def foo 3) foo")
  (test* '() "()")
  (test* 'nil "nil")
  (test* #f "false")
  (test* #t "true")
  (test* #t "(or (and true nil) true)")
  ;; (test* 3 "(:foo {:foo (+ 1 2)})")
  (test* 2 "([1 2 3] 1)")
  (test* 123 "(do 1 2 3 123)")
  (test* (list (make-clojure-symbol #f 'foo)) "'(foo)")
  (test* (make-clojure-vector '(1 2 3 4)) "[1 2 3 (* 2 2)]"))

(test-group "quoting"
  (test* (make-clojure-symbol #f 'foo) "'foo")
  (test* (make-clojure-symbol #f 'bar) "(quote bar)"))

(test-group "keywords"
  (test* (make-clojure-keyword #f 'foo) ":foo")
  (test* (make-clojure-keyword #t 'foo) "::foo")
  (test* (make-clojure-keyword 'foo 'bar) ":foo/bar"))

(test-group "booleans & nil"
  (test* #t "(nil? nil)")
  (test* #f "(nil? false)")
  (test* #f "(nil? true)")
  (test* #t "(false? false)")
  (test* #f "(false? nil)")
  (test* #f "(false? true)")
  (test* #t "(true? true)")
  (test* #f "(true? false)")
  (test* #f "(true? nil)"))

(test-group "conditions"
  (test* #t "(if nil false true)"))

(test-group "qualified symbols"
  (test* list "scheme/list")
  (test* print "chicken/print")
  (test* make-hash-table "srfi-69/make-hash-table")
  (test* "Zm9v"
         "(require 'base64)"
         "(base64/base64-encode \"foo\")"))

(test-group "in-ns"
  (test* #t "(fn? in-ns)")
  (test* 123
         "(in-ns 'foo)"
         "(def hey 123)"
         "(in-ns 'user)"
         "foo/hey"))

(test-group "ns"
  (test* "hey" "(ns foobar) (def baz \"hey\") baz")

  (test* 123
         "(ns abc)
          (def bar 123)
          (ns user)
          abc/bar")

  ;; this one doesn't work yet because already imported modules won't
  ;; import new bindings introduced after the fact
  ;; (test* (list 123 99)
  ;;        "(ns zzz)
  ;;         (def bar 123)
  ;;         (ns user)
  ;;         zzz/bar
  ;;         (ns zzz)
  ;;         (def baz 99)
  ;;         (ns user)
  ;;         (list zzz/bar zzz/baz)")


  ;; note that cljs gets this wrong as well, maybe not worth
  ;; investigating further
  ;; (test-error* "(do (ns fooxoo) (def testo 123) (ns user) testo)")
  )

(test-group "functions"
  (test* "hello, world." "((fn* [x] (str \"hello, \" x \".\")) \"world\")")
  (test* #t "(fn? prn)")
  (test-group "rest argument"
    (test* '(1 2 3) "((fn* [x & y] (scheme/cons x y)) 1 2 3)")
    (test* '(1 2 3) "((fn* [& x] x) 1 2 3)")
    (test-error* "(fn* [&] 1)")
    (test-error* "(fn* [& x y] 1)")
    (test-error* "(fn* [& &] 1)"))
  (test-group "arity overloading"
    (test* '(1 2 (3))
           "(def bar (fn* ([] 1) ([x] x) ([x & y] y)))"
           "(scheme/list (bar) (bar 2) (bar 2 3))"))
  (test-group "named"
    (test* 4 "((fn* self ([x] (self x x)) ([x y] (+ x y))) 2)"))
  (test-group "recur"
    (test* "ok" "((fn* [x] (if (scheme/= x 0) \"ok\" (recur (- x 1)))) 10)"))
  (test* '(1 2)
         "(def x 1)"
         "((fn* [x] (scheme/list user/x x)) 2)"))

(test-group "syntax-quote"
  (test* (make-clojure-symbol 'user 'bar) "`bar")
  (test* (make-clojure-symbol 'foo 'bar) "`foo/bar")
  (test* (make-clojure-symbol 'user 'foo.bar) "`foo.bar")
  (test* (list (make-clojure-symbol 'user 'foo)) "`(foo)")
  (test* (make-clojure-symbol #f 'foo) "`~'foo")
  (test* 3 "`~(+ 1 2)")
  (test* (list 'quote (make-clojure-symbol 'user 'foo)) "``foo")
  (test* 199 "`199")
  (test* (make-clojure-vector (list (make-clojure-symbol 'user 'bla) 3))
         "`[bla ~(+ 1 2)]")
  (test* (make-clojure-map '()) "`{}")
  (test* (make-clojure-map
          (list (cons (make-clojure-symbol 'user 'foo)
                      (make-clojure-symbol 'bar 'baz))
                (cons (make-clojure-symbol 'user 'qux)
                      (make-clojure-symbol #f 'quux))))
         "`{foo bar/baz qux ~'quux}")
  (test* '(1 2 3 4) "`(1 ~@'(2 3) 4)")
  (test* '(nil #t #f) "`(nil true false)"))

(test-group "macros"
  (test* 123
         "(def ^{:macro true} onetwothree" 
         "  (fn* [] `(+ 100 20 3)))"
         "(onetwothree)")
  (test* (make-clojure-symbol #f 'bar)
         "(def ^:macro foo (fn* [x] `'~x)) (foo bar)")
  (test* 123
         "(def ^:macro foo (fn* [x] x))" 
         "(def ^:macro bar (fn* [x] `(foo ~x)))"
         "(def ^:macro baz (fn* [x] `(bar '~x)))" 
         "(baz 123)")

  (test* "expanding to primitives"
         "(def ^:macro check (fn* [name]"
         " `(def ~name \"expanding to primitives\")))"
         "(check foo)"
         "foo"))

(test-group "let*"
  (test* "ok" "(let* [] \"ok\")")
  (test-error* "(let* [x] x)")
  (test-error* "(let* [[x] 1] x)")
  (test* 2 "(let* [x 1 y (+ x 1)] y)")
  (test-error* "(let* [def (fn* [] 123)] (def))")
  (test* 123
         "(def foo 999)"
         "(let* [foo (fn* [] 123)] (foo))")
  (test-error* "(let* [true 1 false 2 nil 3] true)")
  (test* '(1 2)
         "(def x 1)"
         "(let* [x 2] (scheme/list user/x x))"))

(test-group "if, or, and"
  (test* 'nil "(or)")
  (test* #t "(and)")
  (test* 'nil "(if false 1)"))

(test-group "defprotocol, extend, extend-type"
  (test* "bar, bar"
         "(defprotocol Foo"
         "  (foo [x y]))"
         "(extend String"
         "  Foo"
         "  {:foo (fn* [x y] (str x y x))})"
         "(foo \"bar\" \", \")")
  (test* '(1 2)
         "(defprotocol Bar"
         "  (bar [x] [x y]))"
         "(extend-type Symbol"
         "  Bar"
         "  (bar ([x] 1) ([x y] 2)))"
         "(scheme/list (bar 'hey) (bar 'ho 'ho))"))

(test-group "vars"
  (test-error* "#'asdlkajsldf"))

(test-group "meta data"
  (test* (make-clojure-symbol #f 'prn) "(get (meta #'prn) :name)")
  (test* "baz"
         "(def ^:macro foo (fn* [x] `(def ~x)))"
         "(foo ^{:bar \"baz\"} qux)"
         "(get (meta (var qux)) :bar)")
  (test* 3 "(get (meta ^{:foo (+ 1 2)} []) :foo)"))

(test-group "loop*, recur"
  (test* 120
         "(loop* [cnt 5 acc 1]" 
         "  (if (scheme/= 0 cnt)" 
         "      acc" 
         "      (recur (- cnt 1)"
         "             (* acc cnt))))"))

(test-group "instantiation"
  (test* "foo" "(String. \"foo\")")
  (test* "bar" "(clojure.core/String. \"bar\")")
  (test* 'nil "(Nil.)")
  (test* '(#t #f)
         "(let [x \"foo\"]"
         "  (scheme/list"
         "   (identical? x x)"
         "   (identical? x (String. x))))"))

(test-group "deftype"
  (test* '(1 2)
         "(deftype Foo [bar baz])"
         "(def foo (Foo. 1 2))"
         "(scheme/list (.bar foo) (.baz foo))"))

(test-group "private vars"
  (test-error*
   "(ns xfoo)"
   "(def ^:private foo 123)"
   "(def bar 99)"
   "(in-ns 'user)"
   "xfoo/foo"))

(test-group "dynamic vars"
  (test* 10
         "(def ^:dynamic foo 123)"
         "(defn bar [] foo)"
         "(binding [foo 10] (bar))")
  (test* '(123 10)
         "(def ^:dynamic foo 123)"
         "(scheme/list foo (binding [foo 10] foo))")
  (test* '(99 99)
         "(def ^:dynamic foo 123)"
         "(let [foo 99 bar foo] (scheme/list foo bar))"))

(test-group "scheme*"
  (test* '^foo "(scheme* \"'^foo\")")
  (test* '(foo (~bar)) "(scheme* \"'(foo [~bar])\")")
  (test-error*  "(scheme* foo)"))

(test-end "clojure")

(test-exit)
