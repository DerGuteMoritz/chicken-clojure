(module clojure

(clojure-version
 clojure-read clojure-compile clojure-eval clojure-write 
 clojure-compile-file clojure-macro-expand-1 clojure-macro-expand
 clojure-init! clojure-require clojure-refer
              
 clojure-namespace-name clojure-namespace-ref current-clojure-namespace
 clojure-namespace-module-name  switch-current-clojure-namespace!
 clojure-namespace-remove! clojure-namespace-register-var!

 make-clojure-var clojure-var? clojure-var-meta 
 clojure-var-ref clojure-var-deref clojure-var-value

 make-clojure-symbol clojure-symbol?
 clojure-symbol-name clojure-symbol-namespace
 clojure-symbol-meta clojure-symbol->symbol
 clojure-symbol->var-name

 make-clojure-keyword clojure-keyword?
 clojure-keyword-name clojure-keyword-namespace

 make-clojure-map clojure-map? clojure-map-entries

 make-clojure-vector clojure-vector? clojure-vector-elements
 clojure-vector-meta

 clojure-extend clojure-gensym clojure-new clojure-nil?

 +unbound+

 <clojure-object> <clojure-symbol>
 <clojure-var> <clojure-nil>
 <clojure-vector>)

(import chicken scheme)
(use sexpressive sexpressive-clojure srfi-69
     matchable data-structures extras lolevel
     ports srfi-13 srfi-1 files coops coops-protocols coops-primitive-objects)

(reexport (only coops-primitive-objects <string>))

(define clojure-version
  (let-syntax
      ((read-version-file
        (er-macro-transformer
         (lambda (x r c)
           (with-input-from-file "VERSION" read-line)))))
    (read-version-file)))

(define-record clojure-namespace
  name    ; the clojure name
  module  ; the corresponding chicken module

  imports ; a hash-table of alias -> (module . require?)
          ; aliases become prefixed imports in
          ; chicken; if alias is #f then the
          ; value is the list of unprefixed
          ; imports for this module 

  body    ; a queue used for collecting scheme representations of a
          ; namespace body's forms
  vars    ; a hash-table of var names -> var record
  )

(define-record-printer (clojure-namespace ns out)
  (fprintf out "#<clojure-namespace ~A>" (clojure-namespace-name ns)))

(define-record clojure-var
  name namespace value meta dynamic?)

(define-record-printer (clojure-var var out)
  (fprintf out "#<clojure-var ~A (value ~S) (meta ~S)>"
           (clojure-symbol->symbol (clojure-var-name var))
           (clojure-var-value var)
           (clojure-var-meta var)))

(define %make-clojure-var make-clojure-var)

(define +unbound+ '(unbound))

(define (clojure-bound? val)
  (not (eq? val +unbound+)))

(define (make-clojure-var name
                          #!key
                          (value +unbound+)
                          (namespace (current-clojure-namespace))
                          (meta '())
                          dynamic)
  (%make-clojure-var 
   (make-clojure-symbol (clojure-namespace-name namespace) name)
   namespace
   value
   (make-clojure-map
    (fold (lambda (key val map)
            (alist-update! key val map equal?))
          (if (clojure-map? meta)
              (clojure-map-entries meta)
              meta)
          (list +ns-key+ +name-key+)
          (list namespace (make-clojure-symbol #f name))))
   dynamic))

(define (clojure-var-deref var)
  (let ((val (clojure-var-value var)))
    (cond ((eq? +primitive+ val)
           (error 'clojure-var-deref
                  "can't deref primitive var"
                  var))
          ((not (clojure-bound? val))
           (error 'clojure-var-deref
                  "can't deref unbound var"
                  var))
          (else val))))

(define (clojure-var-deref* var-name)
  (clojure-var-deref (clojure-var-ref var-name)))

(define (clojure-nil? x)
  (eq? 'nil x))

(define-class <clojure-object> ())

(define-primitive-class
  <clojure-nil>
  (<clojure-object>)
  clojure-nil?)

(define-primitive-class
  <clojure-symbol>
  (<record> <clojure-object>)
  clojure-symbol?)

(define-primitive-class
  <clojure-keyword>
  (<record> <clojure-object>)
  clojure-keyword?)

(define-primitive-class
  <clojure-vector>
  (<record> <clojure-object>)
  clojure-vector?)

(define-primitive-class
  <clojure-var>
  (<record> <clojure-object>)
  clojure-var?)

(define-for-syntax debug? #f)

(define-syntax debug
  (ir-macro-transformer
   (lambda (x i c)
     (if debug?
         `(let ((val (list . ,(cdr x))))
            (pp val)
            (car val))
         '(void)))))

(define (in-toplevel thunk)
  (let ((mod #f))
    (dynamic-wind
        (lambda ()
          (unless mod
            (set! mod (##sys#current-module)))
          (##sys#switch-module #f))
        thunk
        (lambda ()
          (##sys#switch-module mod)))))

(define compiling?
  (make-parameter #f))

(define output-directory
  (make-parameter "out"))

(define namespaces
  (make-hash-table))

(define clojure-primitives
  '(def fn* let* loop* recur do quote in-ns ns var scheme*
        ;; the following primitives should be implemented in Clojure
        ;; itself later on
        use defprotocol extend-type binding))

(define clojure-reserved-symbols
  '(nil true false))

(define (clojure-reserved-symbol? sym)
  (memq (clojure-symbol-name sym)
        clojure-reserved-symbols))

(define (require* name)
  (handle-exceptions exn
    (if (and (eq? 'require (get-condition-property exn 'exn 'location))
             (equal? (list name) (get-condition-property exn 'exn 'arguments)))
        #f
        (signal exn))
    (in-toplevel
     (lambda ()
       (require name)))))

;; TODO: decouple module-ref from namespaces
(define (module-ref ns-name name #!optional (require? #t))
  (or (##sys#find-module (##sys#resolve-module-name name 'module-ref) #f)
      (and require? (require* name)
           (begin
             (require* (string->symbol (conc name ".import")))
             (module-ref ns-name name #f)))
      (in-toplevel
       (lambda ()
         (eval `(module ,name
                  ()
                  (import (prefix scheme ~)
                          (prefix chicken ~)
                          (only scheme quote)) ; FIXME: this is necessary due to a bug in Chicken
                  (~use (prefix clojure ~))
                  ;; FIXME: this should be a clojure.core var but
                  ;; always be available, even in namespaces created
                  ;; with in-ns
                  (~define (in-ns sym)
                    (~switch-current-clojure-namespace!
                     (~clojure-symbol-name sym)))
                  (~define (~ns) (~clojure-namespace-ref ',ns-name require: #t))))
         (module-ref ns-name name #f)))))

(define (module-name mod)
  (##sys#slot mod 1))

(define (module-exports mod)
  (append (map car (##sys#slot mod 10))
          (map car (##sys#slot mod 11))))

(define (module-export! mod name)
  (let ((internal-name (conc (module-name mod) "#" name)))
    (##sys#setslot
     mod 10
     (alist-update! name
                    (string->symbol internal-name)
                    (##sys#slot mod 10)))))

(define (make-import-form module prefix require?)
  (let ((import    (if require? '~use '~import))
        (specifier (if prefix
                       `(prefix ,module ,(string->symbol (conc prefix "/")))
                       module)))
    (list import specifier)))

(define (clojure-namespace-import! ns module #!key (alias module) require)
  (define (import-module!)
    (debug importing: module alias: alias into: ns require: require)
    (when require
      (require* module))
    (let ((import (make-import-form module alias require)))
      (debug evaling-import: import)
      (eval-in-clojure-namespace import ns)
      (let ((imported-ns (clojure-namespace-ref module)))
        (unless alias
          ;; (debug importing-vars: (hash-table-keys (clojure-namespace-vars imported-ns)))
          (hash-table-for-each
           (clojure-namespace-vars imported-ns)
           (lambda (name var)
             (unless (or (clojure-primitive? var) (clojure-private? var))
               ;; (debug importing-var: var)
               (clojure-namespace-register-var! var)))))
        imported-ns)))

  (hash-table-update!
   (clojure-namespace-imports ns)
   alias
   (lambda (modules)
     (cond ((not alias)
            (import-module!)
            (if (memq module modules)
                modules
                (cons (cons module require) modules)))
           ((eq? module modules)
            (import-module!)
            (cons modules require))
           (else
            (error 'clojure-namespace-import!
                   "alias already refers to different namespace"
                   alias module modules))))
   (lambda ()
     (if (not alias)
         '()
         module))))

;; TODO: allow referring to namespaces rather than modules (requires
;; changing clojure-namespace-import!)

;; FIXME: this function can't be called dynamically when AOT
;; compiling; can this be detected and a corresponding error be
;; raised? should be noted in the docs at least.
(define (clojure-refer . ns-syms)
  (for-each
   (lambda (ns-sym)
     (let ((ns (clojure-namespace-ref (clojure-symbol->namespace-name ns-sym))))
       (clojure-namespace-refer (clojure-namespace-module-name ns))))
   ns-syms)

  (if (compiling?)
      +skip+
      'nil))

(define (clojure-namespace-refer module)
  (clojure-namespace-import!
   (current-clojure-namespace)
   module
   alias: #f
   require: #f))

(define (clojure-namespace-use module)
  (debug using: module ns: (current-clojure-namespace))
  (clojure-namespace-import!
   (current-clojure-namespace)
   module
   alias: #f
   require: #t))

(define (clojure-require module #!optional alias)
  (if alias
      (clojure-namespace-import!
       (current-clojure-namespace)
       module
       alias: alias
       require: #t)
      (begin
        (require* module)
        (require* (string->symbol (conc module ".import")))))

  (if (compiling?)
      +skip+
      'nil))

(define (make-clojure-namespace* name module-name #!optional (require? #t))
  (make-clojure-namespace
   name
   (module-ref name module-name require?)
   (make-hash-table)
   (make-queue)
   (make-hash-table)))

(define (clojure-namespace-ref* name)
  (hash-table-ref/default namespaces name #f))

(define (clojure-namespace-ref name #!key (module name) (require #t))
  (debug namespace-ref: name module: module require: require)
  (or (clojure-namespace-ref* name)
      (let ((ns (make-clojure-namespace* name module require)))
        (debug creating-ns: name)
        (when (zero? (hash-table-size (clojure-namespace-vars ns)))
          (for-each
           (lambda (var-name)
             ;; NOTE: these "foreign" vars can't be dereferenced dynamically
             (clojure-namespace-register-var!
              (make-clojure-var var-name namespace: ns)
              namespace: ns
              export: #f))
           (module-exports (clojure-namespace-module ns))))
        (hash-table-set! namespaces name ns)
        ns)))

(define (clojure-namespace-import-ref ns alias)
  (and-let* ((imports (clojure-namespace-imports ns))
             (ns-name (hash-table-ref/default imports alias #f)))
    (clojure-namespace-ref (car ns-name))))

(define (clojure-namespace-remove! name)
  (hash-table-delete! namespaces name))

;; TODO: check for clashes
(define (clojure-namespace-register-var!
         var
         #!key
         (name (clojure-symbol-name (clojure-var-name var)))
         (namespace (current-clojure-namespace))
         (export #t))
  ;; (debug registering-var: name: name ns: namespace var: var)
  (hash-table-set! (clojure-namespace-vars namespace) name var)
  (when export
    (module-export! (clojure-namespace-module namespace) name)))

(define (clojure-namespace-var-ref name #!optional (ns (current-clojure-namespace)))
  (hash-table-ref/default (clojure-namespace-vars ns) name #f))

(define (clojure-namespace-has-var? name #!optional (ns (current-clojure-namespace)))
  (hash-table-exists? (clojure-namespace-vars ns) name))

(define (clojure-namespace-module-name ns)
  (module-name (clojure-namespace-module ns)))

(define current-clojure-namespace
  (make-parameter #f))

(define +primitive+ '(primitive))

(define (clojure-namespace-import-primitives! ns)
  (for-each
   (lambda (name)
     ;; TODO: use the same var objects for all namespaces
     (clojure-namespace-register-var!
      (make-clojure-var
       name
       value: +primitive+
       namespace: ns
       meta: (list (cons +primitive-key+ #t)))
      namespace: ns))
   clojure-primitives))

;; TODO: can this be removed?
(define (switch-current-clojure-namespace! ns)
  (if (clojure-namespace? ns)
      (begin
        (current-clojure-namespace ns)
        (##sys#switch-module (clojure-namespace-module ns))
        'nil)
      (let ((ns* (clojure-namespace-ref* ns)))
        (if ns*
            (switch-current-clojure-namespace! ns*)
            (let ((ns* (clojure-namespace-ref ns)))
              (clojure-namespace-import-primitives! ns*)
              (switch-current-clojure-namespace! ns*))))))

;; TODO: is this really still required now that Clojure namespaces and
;; Chicken modules are named the same?
(define (clojure-symbol-namespace->module-name sym)
  (or (clojure-symbol-namespace sym)
      (error 'clojure-symbol-namespace->module-name
             "can't get module name of unqualified symbol"
             sym)))

(define (clojure-symbol->namespace-name sym)
  (let ((ns (clojure-symbol-namespace sym))
        (name (clojure-symbol-name sym)))
    (if (and ns name)
        (error 'clojure-symbol->namespace-name "invalid namespace name" sym)
        (or name ns))))

(define (clojure-symbol->var-name sym)
  (cond ((clojure-symbol-namespace sym)
         (error 'clojure-symbol->var-name
                "qualified symbol not allowed as var name"
                sym))
        ((clojure-reserved-symbol? sym)
         (error 'clojure-symbol->var-name
                "reserved symbol not allowed as var name"
                sym))
        (else (clojure-symbol-name sym))))

;; TODO: needed?
(define (namespace-intern name value)
  (when (clojure-symbol-namespace name)
    (error 'namespace-intern "can't intern namespaced symbol" name))
  (hash-table-set! (current-clojure-namespace)
                   (clojure-symbol-name name)
                   (make-parameter value)))

(define (clojure-read #!optional (port (current-input-port)))
  (parameterize ((sexpressive syntax:clojure))
    (read* port)))


(define (emit-var name meta #!optional dynamic?)
  `(~let* ((~var (~make-clojure-var
                  ',name
                  value: ,name
                  meta: ,(clojure-compile meta)
                  dynamic: ,dynamic?)))
          (~clojure-namespace-register-var! ~var)
          ~var))

(define (compile-def def)
  (debug compile-def: (cons 'def def))
  (match def
    (((? clojure-symbol? var) body ...) (=> fail)
     (let* ((name (clojure-symbol->var-name var))
            (meta (clojure-symbol-meta var))
            ;; it's ok to only check dynamicness on creation of a
            ;; var as it can only be declared dynamic at
            ;; definition time and then stays that way
            (dynamic? (meta-ref +dynamic-key+ meta))
            (value (match body
                     ((value)
                      (clojure-compile value))
                     (()
                      '~+unbound+)
                     (else (fail))))
            (value (if dynamic?
                       `(~make-parameter ,value)
                       value)))
       `(~begin
         (~export ,name)
         (~define ,name ,value)
         ,(emit-var name meta dynamic?))))
    (else (error 'compile-def "invalid def syntax" (cons 'def def)))))

(define (boolean->symbol exp)
  (case exp
    ((#t) 'true)
    ((#f) 'false)
    (else (error 'boolean->symbol "not a boolean" exp))))

(define-record clojure-module
  name form)

(define (emit-module ns)
  (let ((body (queue->list (clojure-namespace-body ns)))
        (module-name (clojure-namespace-module-name ns))
        (original-vars (hash-table-fold
                        (clojure-namespace-vars ns)
                        (lambda (name var vars)
                          (if (clojure-namespace-original-var? ns var)
                              (alist-cons name var vars)
                              vars))
                        '())))
    (debug emit-module: module-name vars: original-vars)
    (make-clojure-module
     module-name
     (and (pair? body)
          `(module
            ,module-name
            ()
            (import (prefix scheme ~)
                    (prefix chicken ~)
                    (only scheme quote))
            (~use (prefix clojure ~))
            ,@(hash-table-fold
               (clojure-namespace-imports ns)
               (lambda (alias modules imports)
                 (if alias
                     (cons (make-import-form (car modules) alias (cdr modules))
                           imports)
                     (append
                      (map (lambda (module)
                             (make-import-form (car module) #f (cdr modules)))
                           modules)
                      imports)))
               '())
            (~parameterize ((~current-clojure-namespace 
                             (~clojure-namespace-ref ',(clojure-namespace-name ns) require: #t)))
             ,@body))))))

(define (clojure-gensym sym #!optional gensyms)
  (if gensyms
      (or (alist-ref sym (car gensyms))
          (let ((sym* (make-clojure-symbol #f (gensym sym))))
            (set-car! gensyms (alist-cons sym sym* (car gensyms)))
            sym*))
      (make-clojure-symbol
       #f
       (if sym
           (gensym (clojure-symbol-name sym))
           (gensym)))))

(define (auto-gensym-name sym)
  (and (not (clojure-symbol-namespace sym))
       (let* ((name (symbol->string (clojure-symbol-name sym)))
              (end  (- (string-length name) 1)))
         (and (eq? #\# (string-ref name end))
              (string->symbol (substring name 0 end))))))

(define +nil+ (make-clojure-symbol #f 'nil))

(define (compile-keyword form)
  `(~make-clojure-keyword
    ',(clojure-keyword-namespace form)
    ',(clojure-keyword-name form)))

(define (compile-syntax-quote form)
  (match form
    ((? clojure-symbol?)
     (debug syntax-quote-sym: form)
     (cond ((clojure-reserved-symbol? form)
            (clojure-compile form))
           ((auto-gensym-name form) => 
            (lambda (name)
              `(~clojure-gensym ',name ~gensyms)))
           (else 
            (let ((var (clojure-var-ref* form)))
              `(~make-clojure-symbol
                ',(cond
                   ((clojure-primitive? var)
                    ;; TODO: return 'clojure.core instead?
                    #f)
                   (var
                    (clojure-namespace-name (clojure-var-namespace var)))
                   ((clojure-symbol-namespace form) => identity)
                   (else
                    (clojure-namespace-name (current-clojure-namespace))))
                ',(clojure-symbol-name form))))))
    (((or 'unquote) form)
     (clojure-compile form))
    (((or 'quote 'quasiquote) form)
     `(~list 'quote ,(compile-syntax-quote form)))
    ((? list?)
     (let loop ((forms form))
       (match forms
         (() ''())
         ((('unquote-splicing form) rest ...)
          (list '~append
                (clojure-compile form)
                (loop rest)))
         ((form rest ...)
          (list '~cons
                (compile-syntax-quote form)
                (loop rest))))))
    ((? clojure-vector?)
     `(~make-clojure-vector
       (~list . ,(map compile-syntax-quote
                      (clojure-vector-elements form)))))
    ((? clojure-map?)
     `(~make-clojure-map
       (~list . ,(map (lambda (x)
                        (list '~cons
                              (compile-syntax-quote (car x))
                              (compile-syntax-quote (cdr x))))
                      (clojure-map-entries form)))))
    ((? clojure-keyword?)
     (compile-keyword form))
    ((or (? number?) (? string?))
     form)
    (else
     (error 'compile-syntax-quote "don't know how to syntax-quote" form))))

(define compile-defmacro
  (match-lambda
   ((sym body ...)
    (let* ((sym-name (clojure-symbol-name sym))
           (compiled (clojure-compile
                      (list (make-clojure-symbol #f 'def)
                            sym
                            (cons (make-clojure-symbol #f 'fn*)
                                  body)))))

      (eval-in-clojure-namespace compiled)

      (hash-table-update!/default
       (clojure-namespace-vars (current-clojure-namespace))
       sym-name
       (lambda (meta)
         (make-clojure-map (alist-cons +macro-key+ #t (clojure-map-entries meta))))
       (make-clojure-map '()))
      ;; TODO: also return meta-data update
      compiled))))

(define (clojure-var-ref* sym)
  (let* ((name    (clojure-symbol-name sym))
         (ns-name (clojure-symbol-namespace sym))
         (cur-ns  (current-clojure-namespace))
         (ns      (if ns-name
                      (or (clojure-namespace-import-ref cur-ns ns-name)
                          (and-let* ((ns (clojure-namespace-ref ns-name require: #f)))
                            (unless (eq? (clojure-namespace-name cur-ns) ns-name)
                              (clojure-namespace-import! cur-ns ns-name))
                            ns))
                      cur-ns)))
    (and ns (clojure-namespace-var-ref name ns))))

(define (clojure-var-ref sym)
  (or (clojure-var-ref* sym)
      (error 'clojure-var-ref
             "could not resolve var in this context"
             sym)))

(define (meta-ref key meta)
  (alist-ref key (clojure-map-entries meta) equal?))

(define (var-meta-ref key var)
  (and var (meta-ref key (clojure-var-meta var))))

(define +macro-key+ (make-clojure-keyword #f 'macro))
(define +name-key+ (make-clojure-keyword #f 'name))
(define +ns-key+ (make-clojure-keyword #f 'ns))
(define +primitive-key+ (make-clojure-keyword #f 'primitive))
(define +doc-key+ (make-clojure-keyword #f 'doc))
(define +private-key+ (make-clojure-keyword #f 'private))
(define +dynamic-key+ (make-clojure-keyword #f 'dynamic))

(define (clojure-macro? var)
  (var-meta-ref +macro-key+ var))

(define (clojure-dynamic? var)
  (var-meta-ref +dynamic-key+ var))

(define (clojure-namespace-original-var? ns var)
  (and (not (clojure-primitive? var))
       (let ((var-ns (var-meta-ref +ns-key+ var)))
         (eq? (clojure-namespace-name ns)
              (clojure-namespace-name var-ns)))))

(define (clojure-primitive? var)
  (and var (eq? +primitive+ (clojure-var-value var))))

(define (clojure-private? var)
  (var-meta-ref +private-key+ var))

(define (quote-cdr form)
  (cons (car form)
        (map (lambda (x)
               (list 'quote x))
             (cdr form))))

(define (macro-application? form)
  (let ((head (and (pair? form) (car form))))
    (and (clojure-symbol? head)
         (clojure-macro? (clojure-var-ref* head)))))

(define (clojure-macro-expand-1 form #!optional macro?)
  (if (or macro? (macro-application? form))
      (let ((form (quote-cdr form)))
        (debug expanding: form)
        (clojure-eval form macro-expand: #f))
      form))

(define (clojure-macro-expand form #!optional macro?)
  (if (or macro? (macro-application? form))
      (clojure-macro-expand (clojure-macro-expand-1 form #t))
      form))

;; TODO: find a way to reload modules in interpreted
;; mode when new bindings have been exported from
;; them; is this still needed though with vars?
(define (compile-symbol sym)
  (let ((sym* (clojure-symbol->symbol sym)))
    (case sym*
      ((nil) ''nil)
      ((true) #t)
      ((false) #f)
      (else
       (let* ((var (clojure-var-ref* sym))
              (ns-name (clojure-symbol-namespace sym))
              (sym* (if ns-name
                        (let ((cur-ns (current-clojure-namespace)))
                          (if (eq? ns-name (clojure-namespace-name cur-ns))
                              (clojure-symbol-name sym)
                              (if (clojure-private? var)
                                  (error 'clojure-var-ref
                                         "var is not public"
                                         var)
                                  sym*)))
                        sym*)))
         (cond ((and (not ns-name) (memq sym* (local-scope)))
                (local-var-name sym*))
               ((clojure-dynamic? var)
                (list sym*))
               (else sym*)))))))

(define (compile-fn*-args args)
  (let loop ((args (clojure-vector-elements args)))
    (if (null? args)
        (values '()'())
        (let ((var (clojure-symbol->var-name (car args))))
          (if (eq? '& var)
              (cond ((null? (cdr args))
                     (error 'clojure-compile
                            "missing rest argument name"
                            args))
                    ((not (null? (cddr args)))
                     (error 'clojure-compile
                            "too many arguments after rest argument"
                            args))
                    ((clojure-symbol->var-name (cadr args)) =>
                     (lambda (var)
                       (if (eq? '& var)
                           (error 'clojure-compile
                                  "invalid rest argument name"
                                  var)
                           (values (list var) (local-var-name var))))))
              (receive (locals args) (loop (cdr args))
                (values (cons var locals)
                        (cons (local-var-name var) args))))))))

(define (compile-fn*-tail args body)
  (receive (locals args) (compile-fn*-args args)
    (cons args
          (parameterize ((local-scope (append locals (local-scope))))
            (map clojure-compile body)))))

;; TODO: only define ~recur when it appears in the expression?
(define (compile-fn* form)
  (match form
    (((? clojure-symbol? name) rest ...)
     `(~rec ,(clojure-symbol->var-name name) ,(compile-fn* rest)))
    (((? clojure-vector? args) body ...)
     `(~rec ~recur (~lambda . ,(compile-fn*-tail args body))))
    ((((? clojure-vector? args) body ...) ...)
     `(~rec ~recur (~case-lambda . ,(map compile-fn*-tail args body))))
    (else (error 'clojure-compile
                 "invalid fn* syntax"
                 (cons 'fn* form)))))

(define (ensure-binding-form bindings)
  (unless (clojure-vector? bindings)
    (error 'clojure-compile
           "binding form must be a vector"
           bindings)))

(define local-scope
  (make-parameter '()))

(define (local-var-name sym)
  (string->symbol (string-append "^" (symbol->string sym))))

(define compile-let*
  (match-lambda
   ((bindings body ...)
    (ensure-binding-form bindings)
    (let loop ((bindings (clojure-vector-elements bindings)))
      (match bindings
        (() `(~begin . ,(map clojure-compile body)))
        ((var value rest ...)
         (let ((var (cond ((clojure-symbol? var)
                           (clojure-symbol->var-name var))
                          ((symbol? var) var)
                          (else
                           (error 'clojure-compile
                                  "variable names must be symbols"
                                  bindings)))))
           (parameterize ((local-scope (cons var (local-scope))))
             (cons* '~let
                    (list (list (local-var-name var)
                                (clojure-compile value)))
                    (if (null? rest)
                        (map clojure-compile body)
                        (list (loop rest)))))))
        (else
         (error 'clojure-compile
                "binding form requires an even number elements"
                bindings)))))
   (else (error 'clojure-compile "invalid let* syntax" else))))

;; TODO: implement in Clojure
(define compile-binding
  (match-lambda
   ((bindings body ...)
    (ensure-binding-form bindings)
    (cons* 
     '~parameterize
     (let loop ((bindings (clojure-vector-elements bindings)))
       (match bindings
         (() '())
         ((name value rest ...)
          (cons
           (list
            (cond ((clojure-symbol? name)
                   (let ((var (clojure-var-ref name)))
                     (when (and var (not (clojure-dynamic? var)))
                       (error 'clojure-compile
                              "can't rebind non-dynamic var"
                              var))

                     `(~clojure-var-value
                       ,(clojure-compile
                         (list (make-clojure-symbol #f 'var)
                               name)))))
                  ((symbol? name) name)
                  (else (error 'clojure-compile
                               "variable names must be symbols"
                               bindings)))
            (clojure-compile value))
           (if (null? rest)
               '()
               (loop rest))))
         (else
          (error 'clojure-compile
                 "binding form requires an even number elements"
                 bindings))))
     (map clojure-compile body)))
   (else (error 'clojure-compile "invalid binding syntax" else))))

(define +define-protocol-sym+
  (make-clojure-symbol 'coops-protocols 'define-protocol))

(define (compile-defprotocol form)
  (match form
    (((? clojure-symbol? name) specs ...)
     (let* ((name* (clojure-symbol->var-name name))
            (vars (map (lambda (spec)
                         (cons (clojure-symbol->var-name (car spec))
                               (car spec)))
                       specs))
            (specs (map cdr specs)))
       (match (map (lambda (spec)
                     (receive (span clojure-vector? spec)))
                   specs)
         (((specs docstrings) ...)
          `(~begin
            (~export ,name* . ,(map car vars))
            ,(clojure-compile
              (cons* +define-protocol-sym+
                     name*
                     (map
                      (lambda (var spec)
                        (list (car var)
                              (map
                               (lambda (sig)
                                 (let ((args (clojure-vector-elements sig)))
                                   (if (null? args)
                                       (error 'clojure-compile
                                              "protocol method must take at least one argument"
                                              (cons (car var) spec))
                                       (map clojure-symbol->var-name args))))
                               spec)))
                      vars
                      specs)))
            ,(emit-var name* (clojure-symbol-meta name))
            ,@(map (lambda (var docstring)
                     (let ((meta (clojure-symbol-meta (cdr var))))
                       (emit-var (car var)
                                 (if (null? docstring)
                                     meta
                                     (make-clojure-map
                                      (alist-cons
                                       +doc-key+
                                       (car docstring)
                                       (clojure-map-entries meta)))))))
                   vars
                   docstrings)
            ,name*)))))
    (else (error 'clojure-compile
                 "invalid defprotocol syntax"
                 (cons 'defprotocol form)))))

(define (protocol-has-method? protocol method)
  (any (lambda (spec)
         (eq? (car spec) method))
       (protocol-specs protocol)))

(define (clojure-keyword->clojure-symbol key)
  (make-clojure-symbol
   (clojure-keyword-namespace key)
   (clojure-keyword-name key)))

(define (clojure-extend type . protocols+methods)
  (unless (subclass? (class-of type) <standard-class>)
    (error 'clojure-extend "not a type" type))

  (for-each
   (match-lambda
    ((protocol methods)
     (unless (protocol? protocol)
       (error 'clojure-extend "not a protocol" protocol))
     (unless (clojure-map? methods)
       (error 'clojure-extend "not a method map" methods))
     (for-each
      (match-lambda
       ((method . impl)
        (let* ((name     (if (clojure-symbol? method)
                             (clojure-symbol-name method)
                             (clojure-keyword-name method)))
               (var-name (if (clojure-symbol? method)
                             method
                             (clojure-keyword->clojure-symbol method)))
               (proc     (clojure-var-deref* var-name)))
          (unless (protocol-has-method? protocol name)
            (error 'clojure-extend
                   "protocol doesn't have a method of that name"
                   protocol
                   name))
          ((slot-value proc 'add-new-primary-method)
           (list type) (lambda (next call-next . args)
                         (apply impl args)))
          (set! (protocol-extenders protocol)
                (cons type (protocol-extenders protocol))))))
      (clojure-map-entries methods))))
   (let loop ((pms protocols+methods))
     (cond ((null? pms) '())
           ((null? (cdr pms))
            (error 'clojure-extend
                   "missing method map for protocol"
                   (car pms)))
           (else (receive (pm more-pms)
                     (split-at pms 2)
                   (cons pm (loop more-pms))))))))

(define +fn-sym+
  (make-clojure-symbol 'clojure.core 'fn))

(define compile-extend-type
  (match-lambda
   ((type protocols+methods ...) (=> fail)
    (clojure-compile
     (cons*
      '~clojure-extend
      type
      (let loop ((pms protocols+methods))
        (cond
         ((null? pms) '())
         ((clojure-symbol? (car pms))
          (receive (methods rest) (span list? (cdr pms))
            (cons*
             (car pms)
             (make-clojure-map
              (map
               (match-lambda
                ((method body ...)
                 (cons (list 'quote method)
                       (cons +fn-sym+ body))))
               methods))
             (loop rest))))
         (else (fail)))))))
   (else (error 'clojure-extend-type
                "invalid extend-type syntax"
                else))))

(define (compile-dispatch form)
  (match (clojure-dispatch-value form)
    (('quote sym)
     (if (clojure-symbol? sym)
         ;; TODO: clojure.core instead of #f
         (clojure-compile
          (list (make-clojure-symbol #f 'var) sym))
         (error 'clojure-compile
                "invalid var syntax"
                sym)))
    (else (error 'clojure-compile
                 "invalid dispatch form"
                 form))))

(define +skip+ '(skip))
(define +fn*-sym+ (make-clojure-symbol #f 'fn*))

(define (compile-quote form)
  (match form
    ((? clojure-symbol?)
     `(~make-clojure-symbol
       ',(clojure-symbol-namespace form)
       ',(clojure-symbol-name form)
       ,(clojure-compile (clojure-symbol-meta form))))
    (else (list 'quote form))))

(define compile-loop*
  (match-lambda
   (((? clojure-vector? bindings) body ...)
    (let ((args (let loop ((bindings (clojure-vector-elements bindings)))
                  (if (null? bindings)
                      '()
                      (cons (car bindings) (loop (cddr bindings)))))))
      (compile-let*
       (list bindings 
             `(~letrec ((~recur (,+fn*-sym+ ,(make-clojure-vector args) . ,body) ))
                       (~recur . ,args))))))
   (else (error 'clojure-compile
                "loop* requires vector binding form"))))

(define (clojure-new class args)
  (cond ((subclass? class <clojure-nil>)
         'nil)
        ((subclass? class <primitive-object>)
         (match args
           ((object)
            (if (subclass? (class-of object) class)
                (object-copy object)
                (error 'clojure-new
                       "primitive type constructor argument is not of the required type"
                       object)))
           (else
            (error 'clojure-new
                   "primitive type constructor requires exactly one argument"
                   class
                   args))))
        (else
         (let ((slots (slot-value class 'slots)))
           (if (= (length args) (length slots))
               (apply make class
                      (fold cons* '() slots args))
               (error 'clojure-new
                      "wrong number of arguments for constructor"
                      class
                      (list expected: (length slots))
                      (list got: (length args) args)))))))

(define (instantiation-class sym)
  (let* ((inst (clojure-symbol->symbol sym))
         (inst (symbol->string inst))
         (end  (- (string-length inst) 1)))
    (and (eq? #\. (string-ref inst end))
         (if (clojure-symbol-name sym)
             (make-clojure-symbol
              (clojure-symbol-namespace sym)
              (let ((name (symbol->string (clojure-symbol-name sym))))
                (string->symbol (substring name 0 (- (string-length name) 1)))))
             (make-clojure-symbol #f (string->symbol (substring inst 0 end)))))))

(define +new-sym+
  (make-clojure-symbol 'clojure.core 'new))

(define (compile-instantiation class-name args)
  ;; TODO: the compiled version of +new-sym+ could be cached; where?
  (clojure-compile (cons* +new-sym+ class-name args)))

(define +slot-value-sym+
  (make-clojure-symbol 'coops 'slot-value))

(define (compile-slot-access slot-name args)
  (match args
    ((object)
     (clojure-compile
      (list +slot-value-sym+
            object
            (list 'quote slot-name))))
    (else
     (error 'clojure-compile
            "wrong number of arguments for slot access"
            (cons (string->symbol (conc "." slot-name)) args)))))

(define (slot-access sym)
  (let* ((name (clojure-symbol->symbol sym))
         (name (symbol->string name)))
    (and (eq? #\. (string-ref name 0))
         (string->symbol (substring name 1 (string-length name))))))

(define (compile-fn-application sym args)
  (debug compiling-fn-application: (cons sym args))
  (cond ((slot-access sym) =>
         (lambda (slot-name)
           (compile-slot-access slot-name args)))
        ((instantiation-class sym) =>
         (lambda (class)
           (compile-instantiation class args)))
        (else
         (map clojure-compile (cons sym args)))))

(define compile-scheme*
  (match-lambda
   ((form)
    (if (string? form)
        (call-with-input-string form read)
        (error 'scheme*
               "bad argument type - expected string"
               form)))
   ((forms ...)
    (cons '~begin (map compile-scheme* forms)))))

;; compiles a Clojure form as returned by clojure-read to Scheme
;;
;; TODO: this won't work in normal Scheme modules (i.e. those not
;; automatically defined by module-ref) as they don't import scheme,
;; chicken and clojure.
(define (clojure-compile form #!optional (macro-expand? #t))
  (match form
    ((? eof-object?) (exit))
    ((? string?) form)
    (('quote expr)
     (compile-quote expr))
    ((? number?) form)
    ((? boolean?) form)
    ((? char?) form)
    ('nil ''nil)
    ((? symbol?) form)
    (() '(quote ()))
    (('quasiquote form)
     `(~let ((~gensyms (~list (~list))))
            ,(compile-syntax-quote form)))
    ((? clojure-dispatch?)
     (compile-dispatch form))
    ((? clojure-map?)
     (clojure-compile
      `(~make-clojure-map
        (~list
         . ,(map (lambda (x)
                   `(~cons ,(car x) ,(cdr x)))
                 (clojure-map-entries form)))
        ,(clojure-map-meta form))))
    ((? clojure-vector?)
     `(~make-clojure-vector
       (~list . ,(map clojure-compile (clojure-vector-elements form)))
       ,(clojure-compile (clojure-vector-meta form))))
    ((? clojure-keyword?)
     (compile-keyword form))
    ((? clojure-symbol? sym)
     (compile-symbol sym))
    (((? clojure-keyword? key) map rest ...)
     (clojure-compile (list (make-clojure-symbol 'clojure.core 'get) map key)))
    (((? clojure-vector? vec) idx)
     `(~list-ref
       (~clojure-vector-elements ,(clojure-compile vec))
       ,(clojure-compile idx)))
    (((? clojure-symbol? sym) rest ...)
     (let ((var (clojure-var-ref* sym)))
       (debug sym: sym var: var)
       (cond ((and macro-expand? (clojure-macro? var))
              (clojure-compile (clojure-macro-expand form #t) #f))
             ((clojure-primitive? var)
              (match (cons (clojure-symbol-name sym) rest)
                (('quote expr ignore ...)
                 (compile-quote expr))
                (('in-ns args ...)
                 `(in-ns . ,(map clojure-compile args)))
                (('def def ...)
                 (compile-def def))
                (('do body ...)
                 (cons '~begin
                       (map clojure-compile body)))
                (('fn* rest ...)
                 (compile-fn* rest))
                ;; TODO: implement as function
                (('use ('quote (? clojure-symbol? ns)))
                 (let ((ns-name (clojure-symbol->namespace-name ns)))
                   (clojure-namespace-import! (current-clojure-namespace) ns-name alias: #f)
                   ;; TODO: implement returning void
                   '(~begin)))
                ;; this is replaced with the real thing in clojure.core
                (('ns rest ...)
                 (match rest
                   (((? clojure-symbol? name))
                    (if (compiling?)
                        (let* ((ns-name (clojure-symbol->namespace-name name))
                               (ns      (clojure-namespace-ref ns-name require: #f)))
                          (clojure-namespace-import-primitives! ns)
                          (switch-current-clojure-namespace! ns)
                          +skip+)
                        `(in-ns ,(clojure-compile (list 'quote name)))))
                   (else (error 'clojure-compile "syntax error" (cons 'ns rest)))))
                (('let* rest ...)
                 (compile-let* rest))
                (('defprotocol rest ...)
                 (compile-defprotocol rest))
                (('var args ...)
                 (if (or (null? args) (pair? (cdr args)))
                     (error 'clojure-compile
                            "var needs one argument"
                            form)
                     `(~clojure-var-ref ,(clojure-compile (list 'quote (car args))))))
                (('loop* rest ...)
                 (compile-loop* rest))
                (('recur args ...)
                 (clojure-compile `(~recur . ,args)))
                (('binding rest ...)
                 (compile-binding rest))
                (('scheme* forms ...)
                 (compile-scheme* forms))
                (('extend-type forms ...)
                 (compile-extend-type forms))))
             (else
              (compile-fn-application sym rest)))))
    ((? pair?)
     (map clojure-compile form))
    (else (error 'clojure-compile "unknown form" form))))

(define (write-clojure-module mod output-dir)
  (let* ((module-form (clojure-module-form mod))
         (module-name (symbol->string (clojure-module-name mod)))
         (output-file (make-pathname output-dir module-name "scm")))
    (and module-form
         (begin
           (printf "  compiling ~A to ~A~%~!" module-name output-file)
           (call-with-output-file output-file
             (lambda (out)
               (pp module-form out)
               output-file))))))

(define (clojure-compile-file filename output-dir)
  (printf "compiling ~A~%~!" filename)
  (call-with-input-file filename
    (lambda (port)
      (parameterize ((compiling? #t))
        (in-toplevel
         (lambda ()
           (let loop ()
             (let ((form (clojure-read port)))
               (if (eof-object? form)
                   (cond ((write-clojure-module
                           (emit-module (current-clojure-namespace))
                           output-dir) => list)
                         (else '()))
                   (let ((compiled-form (clojure-compile form)))
                     (cond ((clojure-module? compiled-form)
                            (cond ((write-clojure-module compiled-form output-dir) =>
                                   (lambda (file)
                                     (cons file (loop))))
                                  (else (loop))))
                           ((eq? +skip+ compiled-form) (loop))
                           (else
                            (unless (eq? +skip+ (eval-in-clojure-namespace compiled-form))
                              (queue-add!
                               (clojure-namespace-body (current-clojure-namespace))
                               compiled-form))
                            (loop)))))))))))))

(define (eval-in-clojure-namespace form #!optional (ns (current-clojure-namespace)))
  (let ((mod (##sys#current-module)))
    (dynamic-wind
        (lambda ()
          (##sys#switch-module (clojure-namespace-module ns)))
        (lambda ()
          (eval form))
        (lambda ()
          (##sys#switch-module mod)))))

(define (clojure-init! #!optional bootstrap?)
  (debug bootstrap: bootstrap?)
  (let ((user-ns (clojure-namespace-ref 'user module: 'clojure.user require: #f)))
    (clojure-namespace-import-primitives! user-ns)
    (current-clojure-namespace user-ns)
    (unless bootstrap?
      (clojure-namespace-use 'clojure.core))))

(define (quoted thunk)
  (print
   (string-join
    (string-split (with-output-to-string thunk) "\n")
    "\n; "
    'prefix)))

(define (clojure-eval expr #!key verbose (macro-expand #t))
  (when verbose
    (quoted (cut pp (list compiling: expr))))

  (let* ((scheme-expr (clojure-compile expr macro-expand))
         (_ (when verbose (quoted (cut pp (list evaling: scheme-expr)))))
         (res (eval-in-clojure-namespace scheme-expr)))
    (when verbose
      (newline))
    (if (eq? (void) res)
        'nil
        res)))

(define (clojure-write exp)
  (cond ((clojure-symbol? exp)
         (write (clojure-symbol->symbol exp)))
        ((clojure-keyword? exp)
         (write (clojure-keyword->symbol exp)))
        ((clojure-vector? exp)
         (display #\[)
         (let loop ((els (clojure-vector-elements exp)))
           (unless (null? els)
             (clojure-write (car els))
             (unless (null? (cdr els))
               (display #\space)
               (loop (cdr els)))))
         (display #\]))
        ((clojure-map? exp)
         (display #\{)
         (let ((size (length (clojure-map-entries exp))))
           (for-each
            (match-lambda
             ((key . value)
              (clojure-write key)
              (display #\space)
              (clojure-write value)
              (set! size (- size 1))
              (unless (zero? size)
                (display #\space))))
            (clojure-map-entries exp)))
         (display #\}))
        ((clojure-var? exp)
         (display "#'")
         (display (clojure-symbol->symbol (clojure-var-name exp))))
        ((pair? exp)
         (display #\()
         (let ((size (length exp)))
           (for-each
            (lambda (value)
              (clojure-write value)
              (set! size (- size 1))
              (unless (zero? size)
                (display #\space)))
            exp))
         (display #\)))
        ((boolean? exp)
         (display (if exp "true" "false")))
        ((char? exp)
         (display "\\")
         (display exp))
        (else
         (write exp))))

)
