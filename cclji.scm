(use chicken-syntax clojure extras matchable parley data-structures)

(import-for-syntax chicken)
(begin-for-syntax (use posix))

(define-syntax make-clojure-version-string
  (ir-macro-transformer
   (lambda (x r c)
     (if (file-exists? ".git")
         `(sprintf "~A (~A)"
                   clojure-version
                   ,(with-input-from-pipe "git show --pretty='format:%h'" read-line))
         'clojure-version))))

(define bootstrap? #f)
(define verbose? #f)
(define expressions (make-queue))

(define (fail msg . args)
  (display "Error: " (current-error-port))
  (if (null? args)
      (fprintf (current-error-port) "~A" msg)
      (apply fprintf (current-error-port) msg args))
  (newline (current-error-port))
  (exit 1))

(let loop ((args (command-line-arguments)))
  (match args
    (("-b" rest ...)
     (set! bootstrap? #t)
     (loop rest))
    (("-v" rest ...)
     (set! verbose? #t)
     (loop rest))
    (("-e" rest ...)
     (when (null? rest)
       (fail "Missing argument for -e"))
     (queue-add! expressions (car rest))
     (loop (cdr rest)))
    (())
    (else
     (fail "Unknown option: ~A" (car args)))))

(clojure-init! bootstrap?)

(unless (queue-empty? expressions)
  (for-each
   (lambda (exp)
     (clojure-eval exp))
   (map (lambda (exp)
          (call-with-input-string exp clojure-read))
        (queue->list expressions)))
  (exit))

(printf "Chicken Clojure ~A~%" (make-clojure-version-string))

(define (read/prompt)
  (let* ((prompt (sprintf "~A=> ~!" (clojure-namespace-name (current-clojure-namespace))))
         (input (parley prompt)))
    (if (eof-object? input) "" input)))

(let loop ()
  (condition-case
      (clojure-write
       (clojure-eval
        (call-with-input-string (read/prompt) clojure-read)
        verbose: verbose?))
    ((user-interrupt) (exit 1))
    (exn (sexpressive)
         (print-error-message exn))
    (exn ()
         (print-error-message exn)
         (print-call-chain)))
  (newline)
  (loop))
