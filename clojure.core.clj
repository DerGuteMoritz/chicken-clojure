(ns ^{:doc "The core Clojure language."
      :author "Rich Hickey"}
  clojure.core)

(def refer clojure/clojure-refer)
(refer 'clojure)

(def require
  (fn* [ns]
       (clojure-require
        (clojure-symbol-name ns))))

(require 'coops)
(require 'coops-protocols)
(require 'coops-primitive-objects)
(require 'data-structures)
(require 'srfi-13)

;;;;;;;;;;;;;;;;;;;;;  seq  ;;;;;;;;;;;;;;;;;;;;

(def cons scheme/cons)
(def list scheme/list)


;;;;;;;;;;;;;;;;;;; arithmetic ;;;;;;;;;;;;;;;;;

(def + scheme/+)
(def - scheme/-)
(def * scheme/*)
(def / scheme//)

;;;;;;;;;;; preliminary core macros ;;;;;;;;;;;;

(def ^{:macro true} defn
  (fn* [name & fn-tail]
       `(def ~name (fn* ~@fn-tail))))

(defn ^:macro let [& decl]
  (cons 'let* decl))

(defn ^:macro fn [& decl]
  (cons 'fn* decl))

(def ^:macro if
  (fn*
   ([cond then]
      `(if ~cond ~then nil))
   ([cond then else]
      `(scheme/if (not ~cond) ~else ~then))))

(def ^:macro or
  (fn*
   ([] nil)
   ([x] x)
   ([x y & rest]
      `(let* [x# ~x]
             (if x# x# (or ~y ~@rest))))))

(def ^:macro and
  (fn*
   ([] true)
   ([x] x)
   ([x y & rest]
      `(let* [x# ~x]
             (if x# (and ~y ~@rest) x#)))))

(def ^:macro when
  (fn* [cond & then]
       `(if ~cond (do ~@then))))

(defn symbol
  ([name]
     (let [idx (srfi-13/string-index-right name \/)]
       (if idx
         (symbol (scheme/substring name 0 idx)
                 (scheme/substring
                  name
                  (+ idx 1)
                  (scheme/string-length name)))
         (symbol nil name))))
  ([ns name]
     (make-clojure-symbol
      (if ns (scheme/string->symbol ns) false)
      (scheme/string->symbol name))))
;; (defn ^{:macro true} defmacro [name & fn-tail]
;;   `(do (defn ~name ~@fn-tail)
;;        (alter-meta! (var ~name) assoc :macro true)))

;;;;;;;;;;;;;;;;;;; core types ;;;;;;;;;;;;;;;;;

(defn new [class & args]
  (clojure-new class args))

(def Symbol <clojure-symbol>)
(def String coops-primitive-objects/<string>)
(def Var <clojure-var>)
(def Nil <clojure-nil>)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; arrays ;;;;;;;;;;;;;;;;

(defn ^{:doc "Returns an array cloned from the passed in array"}
  aclone [array]
  (let* [vec (scheme/make-vector (scheme/vector-length array))]
        (chicken/vector-copy! array vec)
        vec))

(defn ^{:doc "Creates a new array."}
  array [& items]
  (scheme/list->vector items))

(defn make-array [n]
  (scheme/make-vector n))

;; TODO: WTF?
;; (defn aget
;;   "Returns the value at the index."
;;   ([array i]
;;      (cljs.core/aget array i))
;;   ([array i & idxs]
;;      (apply aget (aget array i) idxs)))

(defn ^{:doc "Returns the value at the index."}
  aget [array i]
  (scheme/vector-ref array i))

(defn ^{:doc "Sets the value at the index."}
  aset [array i val]
  (scheme/vector-set! array i val))

(defn ^{:doc "Returns the length of the Java array. Works on arrays of all types."}
  alength [array]
  (scheme/vector-length array))

;;;;;;;;;;;;;;;;;;;;;;;;;;; core protocols ;;;;;;;;;;;;;

(defprotocol IFn
  (-invoke
    [this]
    [this a]
    [this a b]
    [this a b c]
    [this a b c d]
    [this a b c d e]
    [this a b c d e f]
    [this a b c d e f g]
    [this a b c d e f g h]
    [this a b c d e f g h i]
    [this a b c d e f g h i j]
    [this a b c d e f g h i j k]
    [this a b c d e f g h i j k l]
    [this a b c d e f g h i j k l m]
    [this a b c d e f g h i j k l m n]
    [this a b c d e f g h i j k l m n o]
    [this a b c d e f g h i j k l m n o p]
    [this a b c d e f g h i j k l m n o p q]
    [this a b c d e f g h i j k l m n o p q s]
    [this a b c d e f g h i j k l m n o p q s t]
    [this a b c d e f g h i j k l m n o p q s t rest]))

(defprotocol ICounted
  (-count [coll] "constant time count"))

(defprotocol IEmptyableCollection
  (-empty [coll]))

(defprotocol ICollection
  (-conj [coll o]))

#_(defprotocol IOrdinal
    (-index [coll]))

(defprotocol IIndexed
  (-nth [coll n] [coll n not-found]))

(defprotocol ISeq
  (-first [coll])
  (-rest [coll]))

(defprotocol ILookup
  (-lookup [o k] [o k not-found]))

(defprotocol IAssociative
  (-contains-key? [coll k])
  #_(-entry-at [coll k])
  (-assoc [coll k v]))

(defprotocol IMap
  #_(-assoc-ex [coll k v])
  (-dissoc [coll k]))

(defprotocol ISet
  (-disjoin [coll v]))

(defprotocol IStack
  (-peek [coll])
  (-pop [coll]))

(defprotocol IVector
  (-assoc-n [coll n val]))

(defprotocol IDeref
 (-deref [o]))

(defprotocol IDerefWithTimeout
  (-deref-with-timeout [o msec timeout-val]))

(defprotocol IMeta
  (-meta [o]))

(defprotocol IWithMeta
  (-with-meta [o meta]))

(defprotocol IReduce
  (-reduce [coll f] [coll f start]))

(defprotocol IEquiv
  (-equiv [o other]))

(defprotocol IHash
  (-hash [o]))

(defprotocol ISeqable
  (-seq [o]))

(defprotocol
    ^{:doc "Marker interface indicating a persistent collection of sequential items"}
  ISequential)

(defprotocol ^{:doc "Marker interface indicating a record object"}
  IRecord)

(defprotocol IPrintable
  (-pr-seq [o opts]))

(defprotocol IPending
  (-realized? [d]))

(defprotocol IWatchable
  (-notify-watches [this oldval newval])
  (-add-watch [this key f])
  (-remove-watch [this key]))

;;;;;;;;;;;;;;;;;;; fundamentals ;;;;;;;;;;;;;;;

(def extend clojure-extend)
(def macroexpand clojure-macro-expand)
(def macroexpand-1 clojure-macro-expand-1)
(def satisfies? coops-protocols/satisfies?)

(defn ^{:doc "Tests if 2 arguments are the same object"}
  identical? [x y]
  (scheme/eq? x y))

(defn ^{:doc "Equality. Returns true if x equals y, false if not. Compares
  numbers and collections in a type-independent manner.  Clojure's immutable data
  structures define -equiv (and thus =) as a value, not an identity,
  comparison."}
  = [x y]
  (-equiv x y))

(defn ^{:doc "Returns true if x is nil, false otherwise."}
  nil? [x]
  (identical? x nil))

(def false? scheme/not)

(defn true? [x]
  (scheme/eq? true x))

(defn not [x]
  (scheme/or (nil? x) (false? x)))



;; (defn type [x]
;;   (js* "(~{x}).constructor"))

;; ;;;;;;;;;;;;;;;;;;; protocols on primitives ;;;;;;;;
;; (declare hash-map list equiv-sequential)

;; (extend Nil
;;   IEquiv
;;   (-equiv [_ o] (nil? o))

;;   ICounted
;;   (-count [_] 0)

;;   IEmptyableCollection
;;   (-empty [_] nil)

;;   ICollection
;;   (-conj [_ o] (list o))

;;   IPrintable
;;   (-pr-seq [o] (list "nil"))

;;   IIndexed
;;   (-nth
;;    ([_ n] nil)
;;    ([_ n not-found] not-found))

;;   ISeq
;;   (-first [_] nil)
;;   (-rest [_] (list))

;;   ILookup
;;   (-lookup
;;    ([o k] nil)
;;    ([o k not-found] not-found))

;;   IAssociative
;;   (-assoc [_ k v] (hash-map k v))

;;   IMap
;;   (-dissoc [_ k] nil)

;;   ISet
;;   (-disjoin [_ v] nil)

;;   IStack
;;   (-peek [_] nil)
;;   (-pop [_] nil)

;;   IMeta
;;   (-meta [_] nil)

;;   IWithMeta
;;   (-with-meta [_ meta] nil)

;;   IReduce
;;   (-reduce
;;     ([_ f] (f))
;;     ([_ f start] start))

;;   IHash
;;   (-hash [o] 0))

;; (extend-type js/Date
;;   IEquiv
;;   (-equiv [o other] (identical? (. o (toString)) (. other (toString)))))

;; (extend-type number
;;   IEquiv
;;   (-equiv [x o] (identical? x o))

;;   IHash
;;   (-hash [o] o))

;; (extend-type boolean
;;   IHash
;;   (-hash [o] (js* "((~{o} === true) ? 1 : 0)")))

;; (extend-type function
;;   IHash
;;   (-hash [o] (goog.getUid o)))


(extend Var
  IMeta
  {:-meta clojure-var-meta})

(extend Symbol
  IMeta
  {:-meta clojure-symbol-meta})

(def
  ^{:doc "Returns the metadata of obj, returns nil if there is no metadata."}
  meta
  (fn* [o]
       (when (satisfies? IMeta o)
         (-meta o))))


;;;;;;;;;;;; type definition ;;;;;;;;;;;;

(defn ^:macro deftype [name fields & specs]
  `(def ~name
     (coops/make-class
      ~name
      (<clojure-object>)
      ~(scheme/map
        clojure-symbol->var-name
        (clojure-vector-elements fields)))))


;;;;;;;;;;  misc / to be cleaned up  ;;;;;;;;;;;

(def newline scheme/newline)
(def print scheme/display)
(def apply scheme/apply)
(def pr clojure-write)

;; FIXME: print properly interspersed with spaces
(defn prn
  ([] (newline))
  ([x] (pr x) (newline))
  ([x y & more]
     (pr x)
     (print " ")
     (pr y)
     (apply prn more)))

(def fn? scheme/procedure?)

(def str data-structures/conc)

;; TODO: implement properly
(defn get
  ([map key]
     (get map key nil))
  ([map key not-found]
     (data-structures/alist-ref key (clojure-map-entries map) scheme/equal? not-found)))
